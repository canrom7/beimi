#beimi 

最新版 0.8.0

开源棋牌游戏，包含麻将、德州、斗地主。首个版本会采用当前最流行的房卡模式。

贝密游戏是一系列棋牌游戏的名称，其中包含麻将、斗地主、德州，目前正在进行UI设计以及后台系统（JAVA）开发，7月中发布0.1.0版本，仅包含前端UI方案，预计8月份发布首个版本，敬请关注！


1. 开发工具：Cocos Creater
1. 开发语言：Java + JavaScript
1. 服务端框架：Spring Boot + MySQL + JPA + [TIO](https://gitee.com/tywo45/t-io)
1. 客户端语言：Cocos Creater/JavaScript

已知问题：
吴志国1月前
首先我先评论下：
本人有一定的开发经验。 是你这么说的吗？适用于学习，首先学习就是新手才会学习,如果贴主真的想让新手学习，为什么要挖那么多坑，来刁难吗？还是为了进群收取费用。
下载下来项目，第一个坑，缺少jar包。
import javax.validation.Valid;import org.springframework.web.servlet.ModelAndView;
类似于这种基础包的问题报错，是贴吧故意诱导新手上钩吗？故意删除Maven 里面的jar不上传吗，还是忘记上传了。
Maven 引入 即可
org.springframework spring-webmvc

javax.validation validation-api 1.1.0.Final
	<dependency>
		<groupId>org.apache.bval</groupId>
		<artifactId>bval-jsr303</artifactId>
		<version>0.5</version>
	</dependency>  
第二个问题，Maven 引入包都是默认版本。我不知道为什么不指定版本？难道发帖的人不知道会导致版本冲突吗？
第三个问题，我先说下，看下代码规范是Eclipse 开发的，由于贴住没有考虑到工具兼容问题，如果有人用IDEA挡下项目，直接会挂掉。用IDEA工具的需要修改配置 org.springframework.boot spring-boot-starter-tomcat compile 
第四个问题：weixin-java-mp
这个包是微信公众号第三方引用的，目前项目中么有用到，可以考虑到扩展性好的话，还是可以加上备注的，以免误导新手。
不过还是非常感谢贴主分型整套代码。 提供了一个CocosCreator_v 工具，不懂C++的朋友们用这个工具可以很快上手,挺厉害的。 项目重构，复用老逻辑。跑起来的。现在在测试期，后期有什么问题，我会尽量评论。

 **
细安装过程请查阅：[安装说明](INSTALL.md)
 ** 


 ** 
美术资源已经移动到 [贝密美术资源](https://gitee.com/beimi_admin/beimi-psd)
 ** 


UI效果图：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192826_1d6f397f_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0709/131509_9a969010_1387891.jpeg "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192845_5526c6bf_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192900_a0dee563_1387891.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195255_ad05a8cb_1387891.jpeg "效果图-纯图片.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195243_390b844f_1387891.jpeg "效果图-纯文字.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195305_d45d0091_1387891.jpeg "效果图-转盘.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195314_2d4e051d_1387891.jpeg "效果图-转盘-领取.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195452_1490c010_1387891.jpeg "满领.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195459_8beb47dd_1387891.jpeg "首充.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195514_8527196e_1387891.jpeg "效果图-商城1.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195506_4dd396c1_1387891.jpeg "效果图-商城.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195522_68ecf0c7_1387891.jpeg "效果图-商城2.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195529_ae17e344_1387891.jpeg "效果图-商城3.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195537_a4b8cde8_1387891.jpeg "效果图-商城4.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0207/195544_7185acc6_1387891.jpeg "效果图-商城5.jpg")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192913_d65bc3cd_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192926_298b49ff_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192938_28a2548f_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192954_eeba8b49_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/193004_066f1ad5_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0709/131509_9a969010_1387891.jpeg "在这里输入图片标题")
